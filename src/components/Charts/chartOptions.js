const proposalResultOptions = {
  chart: {
    stacked: true,
      stackType: '100%',
      height: '200px',
      toolbar: {
      show: false,
    }
  },
  plotOptions: {
    bar: {
      horizontal: true,
    },
  },
  tooltip: {
    enabled: false,
      onDatasetHover: {
      highlightDataSeries: false,
    },
  },
  fill: {
    colors: ['#00ff00','#ff0000'],
      opacity: 1
  },
  dataLabels: {
    style: {
      fontSize: '18px',
    },
    dropShadow: {
      enabled: true,
        top: 0,
        left: 0,
        blur: 2,
        opacity: 0.2
    }
  },
  legend: {
    show: false
  },
  states: {
    normal: {
      filter: {
        type: 'none',
          value: 0,
      }
    },
    hover: {
      filter: {
        type: 'none',
          value: 0,
      }
    },
    active: {
      filter: {
        type: 'none',
          value: 0,
      }
    },
  },
  yaxis: {
    show: false,
    labels: {
      show: false,
    },
  },
  xaxis: {
    show: false,
      labels: {
      show: false,
    },
    axisBorder: {
      show: false,
    },
    axisTicks: {
      show: false,
    },
    crosshairs: {
      show: false,
    },
  },
  animations: {
    enabled: false,
  }
};
const proposalResultDonutOptions = {
  labels: ["Ja", "Nein"],
  legend: {
    show: true,
    position: 'bottom',
    horizontalAlign: 'center',
  },
  theme: {
    mode: 'light',
    palette: 'palette6',
  }
};
const genderDistributionOptions = {
  labels: ["Frauen", "Männer"],
  legend: {
    show: true,
    position: 'bottom',
    horizontalAlign: 'center',
  },
  theme: {
    mode: 'light',
    palette: 'palette5',
  }
};

const ageDistributionOptions = {
  xaxis: {
    labels: {
      rotate: -45
    },
    categories: ["18 - 39 Jahre", "40 - 64 Jahre", "65 - 79 Jahre", "80 + Jahre"],
  },
  legend: {
    show: true,
    position: 'bottom',
    horizontalAlign: 'center',
  },
  theme: {
    mode: 'light',
    palette: 'palette5',
  }
};

export default {
  proposalResultOptions,
  proposalResultDonutOptions,
  genderDistributionOptions,
  ageDistributionOptions
}
