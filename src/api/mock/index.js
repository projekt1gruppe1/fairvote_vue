import proposals from './data/proposals.json';
import users from './data/users.json';
import news from './data/news.json';
import results from './data/votes.json';
import cantons from './data/cantonResults.json';

const apiTimeout = 1000;

const fetch = (mockData, time = 0) => new Promise((resolve) => {
  setTimeout(() => {
    resolve(mockData);
  }, time);
});

const error = (message, time = 0) => new Promise((resolve, reject) => {
  setTimeout(() => {
    reject(message);
  }, time);
});

function pick(obj, keys) {
  return keys.map(k => (k in obj ? { [k]: obj[k] } : {}))
    .reduce((res, o) => Object.assign(res, o), {});
}

// credits: https://stackoverflow.com/a/49434653
function randn_bm(min, max, skew) {
  let u = 0; let
    v = 0;
  while (u === 0) u = Math.random(); // Converting [0,1) to (0,1)
  while (v === 0) v = Math.random();
  let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);

  num = num / 10.0 + 0.5; // Translate to 0 -> 1
  if (num > 1 || num < 0) num = randn_bm(min, max, skew); // resample between 0 and 1 if out of range
  num = Math.pow(num, skew); // Skew
  num *= max - min; // Stretch to fill range
  num += min; // offset to min
  return num;
}

export default {
  fetchProposalList(isArchive) {
    const status = isArchive ? 20 : 10;

    return fetch(proposals.data
      .filter(proposal => proposal.Status === status)
      .map(row => pick(row, ['id', 'name', 'effectiveDate'])), apiTimeout);
  },
  fetchProposal(id) {
    return fetch(proposals.data.filter(proposal => proposal.id === id)[0], apiTimeout);
  },
  fetchProposalResults(id) {
    return fetch(results.data.filter(result => result.id === id)[0], apiTimeout);
  },
  fetchCantonResult(proposalId, cantonId) {
    // instead of using random numbers for the canton-results,
    // we use skewed normal distribution with a min and a max
    // value for more realistic fake results
    return fetch({ yes: randn_bm(200, 800, 1.1), id: cantonId }, apiTimeout);
  },
  fetchCantonResults(proposalId) {
    return fetch(cantons.data, apiTimeout);
  },
  fetchNewsList() {
    return fetch(news.data, apiTimeout);
  },
  login(email, password) {
    const user = users.data.filter(u => u.email === email && u.password === password)[0];

    if (user) {
      return fetch(user, apiTimeout);
    }
    return error('Login failed!', apiTimeout);
  },
  vote(code, decision, gender, cantonOfResidence) {
    let errorMsg;

    // check if parameters are set
    if (code === undefined) errorMsg = 'Code cannot be undefined';
    if (decision === undefined) errorMsg = 'Decision cannot be undefined';
    if (gender === undefined) errorMsg = 'Gender cannot be undefined';
    if (cantonOfResidence === undefined) errorMsg = 'CantonOfResidence cannot be undefined';

    // validate parameters
    if (code.length !== 16) errorMsg = 'Code has an invalid format (length should equal 16).';
    if (!(decision >= 1 && decision <= 3)) errorMsg = 'Decision has an invalid format (only the values 1, 2 & 3 are allowed).';
    if (!(gender === 'f' || gender === 'm')) errorMsg = 'Gender has an invalid format (only the values \'m\' of \'f\' are allowed).';
    if (cantonOfResidence.length !== 2) errorMsg = 'Canton of Residence has an invalid format (the two-letter-code of a canton is required. Eg: ZH, BE, AG, etc.).';

    if (errorMsg !== undefined) return error(`Vote failed: ${errorMsg}`, apiTimeout);

    // if we reach this point, the api call has been successful
    return fetch('Vote has successfully been cast.', apiTimeout);
  },

  // Admin Requests
  adminFetchProposalList() {
    return fetch(proposals.data, apiTimeout);
  },
  adminFetchProposal(id) {
    return fetch(proposals.data.filter(proposal => proposal.id === id)[0], apiTimeout);
  },
};
