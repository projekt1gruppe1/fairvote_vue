const url = 'http://api.fairvote.ch';

export default {
  fetchProposalList(isArchive) {
    if (isArchive) return this.$http.get(`${url}/proposals/archived`);
    return this.$http.get(`${url}/proposals/recent`);
  },
  fetchProposal(proposalId) {
    return this.$http.get(`${url}/proposals/${proposalId}`);
  },
  fetchProposalResults(proposalId) {
    return this.$http.get(`${url}/results/${proposalId}`);
  },
  fetchCantonResult(proposalId, cantonId) {
    return this.$http.get(`${url}/results/${proposalId}/canton/${cantonId}`);
  },
  fetchCantonResults(proposalId) {
    return this.$http.get(`${url}/results/${proposalId}/cantons`);
  },
  fetchNewsList() {
    return this.$http.get(`${url}/news/recent`);
  },
  login(email, password) {
    return this.$http.post(`${url}/login`, { email, password });
  },
  vote(code, decision, gender, cantonOfResidence) {
    return this.$http.post(`${url}/vote`, {
      code, decision, gender, cantonOfResidence,
    });
  },
};
