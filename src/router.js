import Vue from 'vue';
import store from './store';
import Router from 'vue-router';
import Home from '@/views/Home.vue';
import Ergebnisse from '@/views/Ergebnisse.vue';
import Liste from '@/views/Liste.vue';
import Details from '@/views/Details.vue';
import Error404 from '@/views/Error404.vue';
import Abstimmung from '@/views/Abstimmung.vue';
import Danke from '@/views/Danke.vue';
import Login from '@/views/Login.vue';
import Help from '@/views/Help.vue';
import Impressum from '@/views/Impressum.vue';
import Evoting from '@/views/Evoting.vue';
import Code from '@/views/Code.vue';
import List from '@/views/Admin/List.vue';
import Create from '@/views/Admin/Create.vue';
import Update from '@/views/Admin/Update.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/ergebnisse/:id',
      name: 'ergebnisse',
      component: Ergebnisse,
      props: true,
    },
    {
      path: '/vorlagen/:type',
      name: 'liste',
      component: Liste,
      props: true,
    },
    {
      path: '/zugang/:id',
      name: 'code',
      component: Code,
      props: true,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/abstimmung/:id',
      name: 'abstimmung',
      component: Abstimmung,
      props: true,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/vorlagen/:id',
      name: 'proposalDetails',
      component: Details,
      props: true,
    },
    {
      path: '/danke/:id',
      name: 'danke',
      component: Danke,
      props: true,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      props: true,
    },
    {
      path: '/help',
      name: 'help',
      component: Help,
      props: true,
    },
    {
      path: '/impressum',
      name: 'impressum',
      component: Impressum,
      props: true,
    },
    {
      path: '/evoting',
      name: 'evoting',
      component: Evoting,
      props: true,
    },
    // Admin Pages
    {
      path: '/admin/proposals',
      name: 'proposalList',
      component: List,
      meta: {
        requiresAdmin: true,
      },
    },
    {
      path: '/admin/proposals/create',
      name: 'createProposal',
      component: Create,
      meta: {
        requiresAdmin: true,
      },
    },
    {
      path: '/admin/proposals/update/:id',
      name: 'updateProposal',
      props: true,
      component: Update,
      meta: {
        requiresAdmin: true,
      },
    },
    {
      path: '*',
      name: '404',
      component: Error404,
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.loggedIn) {
      next('/login');
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.requiresAdmin)) {
    if (store.state.user && store.state.user.isAdmin) {
      next();
    } else {
      next('/home');
    }
  } else {
    next();
  }
});

export default router;
