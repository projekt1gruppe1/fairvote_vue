import Vue from 'vue';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faPollH, faRobot, faVoteYea, faQuestionCircle, faInfoCircle, faEdit, faTrashAlt,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';


import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import VueAxios from 'vue-axios';
import axios from './api/axiosConfig';

import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';

library.add(faPollH);
library.add(faRobot);
library.add(faVoteYea);
library.add(faQuestionCircle);
library.add(faInfoCircle);
library.add(faEdit);
library.add(faTrashAlt);
Vue.component('icon', FontAwesomeIcon);

Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  axios,
  render: h => h(App),
}).$mount('#app');
