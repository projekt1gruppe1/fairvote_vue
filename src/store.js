import Vue from 'vue';
import Vuex from 'vuex';
import client from 'api-client';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

function pick(obj, keys) {
  return keys.map(k => (k in obj ? { [k]: obj[k] } : {}))
    .reduce((res, o) => Object.assign(res, o), {});
}

export default new Vuex.Store({
  state: {
    user: null,
    loggedIn: false,
    proposals: [],
  },
  mutations: {
    setProposals(state, proposals) {
      state.proposals = proposals;
    },
    loginSuccess(state, user) {
      state.loggedIn = true;
      state.user = user;
    },
    loginFailure(state) {
      state.loggedIn = false;
      state.user = null;
    },
    logout(state) {
      state.loggedIn = false;
      state.user = null;
    },
  },
  actions: {
    fetchProposals({ commit }) {
      return client
        .fetchProposalList()
        .then((response) => {
          commit('setProposals', response.data.map(row => pick(row, ['id', 'name', 'effectiveDate'])));
        });
    },
    fetchProposal({ commit }, { systemName }) {
      return client
        .fetchProposal(systemName)
        .then(proposals => commit('setProposals', proposals.data));
    },
    login({ commit, dispatch }, { username, password }) {
      return new Promise((resolve, reject) => {
        client.login(username, password)
          .then(
            user => {
              commit('loginSuccess', user);
              resolve();
            },
            error => {
              commit('loginFailure', error);
              reject(error);
              //dispatch('alert/error', error, { root: true });
            }
          );
      })

    },
    logout({ commit }) {
      commit('logout');
    },
  },
  plugins: [createPersistedState()],
});
